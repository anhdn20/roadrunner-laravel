# Roadrunner - Laravel

`Cap nhat lan cuoi: 06/06/2022 Boi anhdn20`

## Cach Build Roadrunner - Laravel tren Window

Nguon huong dan : [Link](https://learnku.com/articles/63557)

### [ ] Chuan bi

-	**Composer:** [https://getcomposer.org/download/](https://getcomposer.org/download/)
-	**Luu y:** `Laravel - php 7.3 | 8.0`
-	**Cau hinh file php.ini:** tim kiem `extension=php_sockets.dll` neu no ton tai thi xoa dau `;` dang truoc de mo nguoc lai khong tim thay thi them vao

    ![Tutorial](</image/1.png>)

### [ ] Tien hanh

- Tao moi project laravel

  ```bash
    composer create-project laravel/laravel:^8.* project-roadrunner-laravel
  ```
- Cai spiral/roadrunner-laravel package

  ```bash
    composer require spiral/roadrunner-laravel "^3.7.0"
  ```
- Publish package len, chay lenh `php artisan vendor:publish` chon `Spiral\RoadRunnerLaravel\ServiceProvider` se co danh sach so tuong ung hien ra, sau khi chon thi se co ket qua nhu sau

    ![Tutorial](</image/2.png>)

- `rr.yaml.dist`  doi ten thanh `.rr.yaml`

- Tai tep thuc thi cho Roadrunner `(o day no se la file rr.exe)` bang cach chay 
    ```
    .\vendor\bin\rr get-binary
    ``` 
    hoac
    tai o [https://github.com/spiral/roadrunner-binary/releases](https://github.com/spiral/roadrunner-binary/releases) va dat tai thu muc goc

- O day se cau hinh lai file .rr.yaml xoa di cau hinh va giu lai nhung gi can thiet nhu sau:
    ```
    http:
    address: 127.0.0.1:8080

    workers:
        # linux版本
        #command: "php ./vendor/bin/rr-worker start --relay-dsn unix:///var/run/rr-relay.sock"
        #relay: "unix:///var/run/rr-relay.sock"

        # windows版本
        command: "php vendor/spiral/roadrunner-laravel/bin/rr-worker start pipes"

        # connection method (pipes, tcp://:9000, unix://socket.unix). default "pipes"
        relay: "pipes"
        user: ""
        pool:
        numWorkers: 4
        maxJobs: 0
        allocateTimeout: 60
        destroyTimeout: 60

    static:
    dir: "public"
    forbid: [ ".php", ".htaccess" ]
    request:
        "Example-Request-Header": "Value"
    response:
        "X-Powered-By": "RoadRunner"
    ```
- Bat dau start serve:
    ```
    rr.exe serve -v -d -c ./.rr.yaml
    ```

### [ ] Luu y nho

- Khi chung ta cap nhat bat ki file nao se thay chua duoc luu tu dong luc nay chung ta noi them mot doan vao file `.rr.yaml` nhu sau:

    ```
    reload:
    interval: 1s
    patterns: [ ".php" ]
    services:
        http:
        # 配置的路由文件重新加载，按需配置，不建议配置为""
        dirs: [ "routes" ]
        recursive: true
    ```
